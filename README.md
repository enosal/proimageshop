proImageShop
============

This is a javaFX image editing application that allows you to add a photo, undo/redo an action up to 10 times, select an area and apply effects, use a dropper.

Advanced Java, Summer 2016
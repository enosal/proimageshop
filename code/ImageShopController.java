package code;

import java.awt.image.BufferedImage;
import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Rectangle2D;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.*;
import javafx.scene.effect.DropShadow;
import javafx.scene.effect.GaussianBlur;
import javafx.scene.effect.SepiaTone;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import javafx.stage.FileChooser;

import javax.imageio.ImageIO;

/**
 * Sample Skeleton for 'imageshop.fxml' Controller Class
 */


public class ImageShopController implements Initializable {


    public enum Mouse { SEL, DRAW, DROP, NONE }
    public enum Pen { CIR, SQR }
    public enum FilterStyle { DRK, BRT, SAT, DESAT, RED, GREEN, BLUE, GRAY, IVRT }
    private int penSize = 50;
    private Mouse mouseTool = Mouse.NONE;
    private Pen penStyle = Pen.CIR;
    private FilterStyle mFilterStyle = FilterStyle.DRK;

    private double xPos, yPos, hPos, wPos;     // for mouse clicks
    private Color mColor;

    ArrayList<Shape> drawShapes = new ArrayList<>(1000);
    ArrayList<Shape> highlightedShapes = new ArrayList<>(1000);

    SnapshotParameters snapshotParameters;
    Image snapshot;

    /////////////
    // MEMBERS //
    /////////////

    @FXML
    private ImageView imgView;

    @FXML
    private AnchorPane ancRoot;

    @FXML
    private ComboBox<String> cboFilter;

    @FXML
    private ToggleButton tgbSelect;

    @FXML
    private ToggleButton tgbDraw;

    @FXML
    private ToggleButton tgbDropper;

    @FXML
    private Button btnBlur;

    @FXML
    private Button btnShadow;

    @FXML
    private Button btnSepia;

    @FXML
    private Button btnFilter;

    @FXML
    private Button btnUndo;

    @FXML
    private Button btnRdo;

    @FXML
    private ColorPicker cpkColor;

    @FXML
    private Slider sldSize;

    @FXML
    private Button btnBucket;

    @FXML
    private ToggleButton tgbSquare;

    @FXML
    private ToggleButton tgbCircle;

    private ToggleGroup mToggleGroupPen = new ToggleGroup();

    private ToggleGroup mToggleGroupTool = new ToggleGroup();

    /////////////
    // ACTIONS //
    /////////////


    //http://java-buddy.blogspot.com/2013/01/use-javafx-filechooser-to-open-image.html
    @FXML
    void mnuOpenAction(ActionEvent event) {
        Cc.getInstance().setImgView(this.imgView);

        FileChooser fileChooser = new FileChooser();

        //Set extension filter
        FileChooser.ExtensionFilter extFilterJPG = new FileChooser.ExtensionFilter("JPG files (*.jpg)", "*.JPG");
        FileChooser.ExtensionFilter extFilterPNG = new FileChooser.ExtensionFilter("PNG files (*.png)", "*.PNG");
        fileChooser.getExtensionFilters().addAll(extFilterJPG, extFilterPNG);

        //Show open file dialog
        File file = fileChooser.showOpenDialog(null);

        try {
            BufferedImage bufferedImage = ImageIO.read(file);

            Cc.getInstance().setImageAndRefreshView(SwingFXUtils.toFXImage(bufferedImage, null));
            snapshotParams();
            disableImageBtns(false);

        } catch (Exception ex) {
            System.out.println("no input file");
            //Logger.getLogger(ImageShopController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @FXML
    void mnuQuitAction(ActionEvent event) {System.exit(0);    }

    @FXML
    void mnuUndo(ActionEvent event) {Cc.getInstance().undo();    }

    @FXML
    void mnuRedo(ActionEvent event) {        Cc.getInstance().redo();    }

    @FXML
    void applyFilter() {
        //Bucket can only be used if selection tool is on
        if (mouseTool == Mouse.SEL & highlightedShapes.size() != 0) {
            Image transformImage;
            switch (mFilterStyle) {

                case DRK:
                    transformImage = Cc.getInstance().transform(Cc.getInstance().getImg(),
                            (x, y, c) -> (x > xPos && x < wPos)
                                    && (y > yPos && y < hPos) ? c.deriveColor(0, 1, .5, 1) : c
                    );
                    break;

                case BRT:
                    transformImage = Cc.getInstance().transform(Cc.getInstance().getImg(),
                            (x, y, c) -> (x > xPos && x < wPos)
                                    && (y > yPos && y < hPos) ? c.deriveColor(0, 1, 2, 1) : c
                    );
                    break;


                case IVRT:
                    transformImage = Cc.getInstance().transform(Cc.getInstance().getImg(),
                            (x, y, c) -> (x > xPos && x < wPos)
                                    && (y > yPos && y < hPos) ? c.invert() : c
                    );
                    break;


                case SAT:
                    transformImage = Cc.getInstance().transform(Cc.getInstance().getImg(),
                            (x, y, c) -> (x > xPos && x < wPos)
                                    && (y > yPos && y < hPos) ? c.deriveColor(0, 1.0 / .1, 1.0, 1.0) : c
                    );
                    break;

                case DESAT:
                    transformImage = Cc.getInstance().transform(Cc.getInstance().getImg(),
                            (x, y, c) -> (x > xPos && x < wPos)
                                    && (y > yPos && y < hPos) ? c.deriveColor(0, .5, 1.0, 1.0) : c
                    );
                    break;

                case RED:
                    transformImage = Cc.getInstance().transform(Cc.getInstance().getImg(),
                            (x, y, c) -> (x > xPos && x < wPos)
                                    && (y > yPos && y < hPos) ? c.interpolate(javafx.scene.paint.Color.RED, 0.5) : c
                    );
                    break;

                case GREEN:
                    transformImage = Cc.getInstance().transform(Cc.getInstance().getImg(),
                            (x, y, c) -> (x > xPos && x < wPos)
                                    && (y > yPos && y < hPos) ? c.interpolate(javafx.scene.paint.Color.GREEN, 0.5) : c
                    );
                    break;

                case BLUE:
                    transformImage = Cc.getInstance().transform(Cc.getInstance().getImg(),
                            (x, y, c) -> (x > xPos && x < wPos)
                                    && (y > yPos && y < hPos) ? c.interpolate(javafx.scene.paint.Color.BLUE, 0.5) : c
                    );
                    break;


                case GRAY:
                    transformImage = Cc.getInstance().transform(Cc.getInstance().getImg(),
                            (x, y, c) -> (x > xPos && x < wPos)
                                    && (y > yPos && y < hPos) ? c.grayscale() : c
                    );
                    break;


                default:
                    //make darker
                    transformImage = Cc.getInstance().transform(Cc.getInstance().getImg(),
                            (x, y, c) -> (x > xPos && x < wPos)
                                    && (y > yPos && y < hPos) ? c.deriveColor(0, 1, .5, 1) : c
                    );
                    break;
            } //end switch

            //Refresh view
            Cc.getInstance().setImageAndRefreshView(transformImage);

        } //end highlighted shapes if

        //Remove selected area
        ancRoot.getChildren().removeAll(highlightedShapes);
        highlightedShapes.clear();

    }

    @FXML
    void applyBucket() {
        //Bucket can only be used if selection tool is on
        if (mouseTool == Mouse.SEL && highlightedShapes.size() != 0) {
            Image transformImage = Cc.getInstance().transform(Cc.getInstance().getImg(),
                    (x, y, c) -> (x > xPos && x < wPos)
                            && (y > yPos && y < hPos) ? cpkColor.getValue() : c
            );

            //Remove selection
            ancRoot.getChildren().removeAll(highlightedShapes);
            highlightedShapes.clear();

            //Refresh view
            Cc.getInstance().setImageAndRefreshView(transformImage);
        }
    }


    @FXML
    void applySepia() {
        Image transformImage = Cc.getInstance().transform(Cc.getInstance().getImg(), new SepiaTone());
        Cc.getInstance().setImageAndRefreshView(transformImage);
    }

    @FXML
    void applyBlur() {
        Image transformImage = Cc.getInstance().transform(Cc.getInstance().getImg(), new GaussianBlur());
        Cc.getInstance().setImageAndRefreshView(transformImage);
    }

    @FXML
    void applyShadow() {
        Image transformImage = Cc.getInstance().transform(Cc.getInstance().getImg(), new DropShadow());
        Cc.getInstance().setImageAndRefreshView(transformImage);
    }



    //##################################################################
    //INITIALIZE METHOD
    //see: http://docs.oracle.com/javafx/2/ui_controls/jfxpub-ui_controls.htm
    //##################################################################
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        //Initialize ImageView
        Cc.getInstance().setImgView(this.imgView);
        snapshotParams();
        Cc.getInstance().setImageAndRefreshView(snapshot);

        //Set up color picker
        mColor = cpkColor.getValue();

        //Button set-up
        disableDrawSel(true, true);
        disableImageBtns(true);

        //Pen toggle group
        tgbCircle.setToggleGroup(mToggleGroupPen);
        tgbSquare.setToggleGroup(mToggleGroupPen);

        //Tool toggle groups
        tgbSelect.setToggleGroup(mToggleGroupTool);
        tgbDraw.setToggleGroup(mToggleGroupTool);
        tgbDropper.setToggleGroup(mToggleGroupTool);


        //Pen Shape selector
        mToggleGroupPen.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
            @Override
            public void changed(ObservableValue<? extends Toggle> observable, Toggle oldValue, Toggle newValue) {
                if (newValue == tgbCircle) {
                    mouseTool = Mouse.DRAW;
                    penStyle = Pen.CIR;
                } else if (newValue == tgbSquare) {
                    mouseTool = Mouse.DRAW;
                    penStyle = Pen.SQR;
                } else {
                    mouseTool = Mouse.NONE;
                    penStyle = Pen.CIR;
                }
            } //end changed method
        }); //end Pen Toggle listener

        //Tool selector
        mToggleGroupTool.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
            @Override
            public void changed(ObservableValue<? extends Toggle> observable, Toggle oldValue, Toggle newValue) {
                if (newValue == tgbSelect) {
                    mouseTool = Mouse.SEL;
                    disableDrawSel(true, false);
                } else if (newValue == tgbDraw) {
                    mouseTool = Mouse.DRAW;
                    disableDrawSel(false, true);
                } else if (newValue == tgbDropper){
                    mouseTool = Mouse.DROP;
                    disableDrawSel(true, true);
                } else {
                    mouseTool = Mouse.NONE;
                    disableDrawSel(true, true);
                }
            } //end changed method
        }); //end Tool Toggle listener


        //ImageView Mouse PRESSED
        imgView.addEventFilter(MouseEvent.MOUSE_PRESSED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent me) {
                //Get coordinates
                xPos = (int) me.getX();
                yPos = (int) me.getY();

                me.consume();
            } //end handle method
        }); //end Mouse CLICKED listener

        //ImageView MOUSE RELEASED
        imgView.addEventFilter(MouseEvent.MOUSE_RELEASED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent me) {
                //DRAW
                if (mouseTool == Mouse.DRAW) {
                    snapshotParams();
                    Cc.getInstance().setImageAndRefreshView(snapshot);
                    ancRoot.getChildren().removeAll(drawShapes);
                    drawShapes.clear();
                }
                //SELECT
                else if (mouseTool == Mouse.SEL) {
                    ancRoot.getChildren().removeAll(highlightedShapes);
                    highlightedShapes.clear();

                    wPos = (int) me.getX();
                    hPos = (int) me.getY();

                    Shape highlightedArea = new Rectangle(xPos, yPos, wPos - xPos, hPos - yPos);
                    highlightedArea.getStyleClass().add("highlightedArea");

                    ancRoot.getChildren().add(highlightedArea);
                    highlightedShapes.add(highlightedArea);

                }
                //DROPPER
                else if (mouseTool == Mouse.DROP) {
                    ancRoot.getChildren().removeAll(drawShapes);
                    drawShapes.clear();
                    cpkColor.setValue(Cc.getInstance().getImg().getPixelReader().getColor((int) me.getX(), (int) me.getY()));
                }//end mouse tool if

                me.consume();
            } //end handle method
        }); //end Mouse RELEASED listener


        imgView.addEventFilter(MouseEvent.MOUSE_DRAGGED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent me) {
                if (mouseTool == Mouse.DRAW) {
                    xPos = me.getX();
                    yPos = me.getY();
                    drawMouse();
                }
                me.consume();
            } //end handle method
        }); //end Mouse DRAGGED listener


        //Color picker
        cpkColor.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                mColor = cpkColor.getValue();
            }
        });

        //Pen size slider
        sldSize.valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                double temp  = (Double) newValue; //automatic unboxing
                penSize = (int) Math.round(temp) + 1; //Pen size is 1 when slider is at the leftmost
            }
        });


        //Combo box - Filter
        cboFilter.getItems().addAll(
            "Darker",
            "Brighter",
            "Saturate",
            "Desaturate",
            "Redscale",
            "Greenscale",
            "Bluescale",
            "Grayscale",
            "Invert"
        );

        //Combox box Filter listener
        cboFilter.valueProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                switch (newValue){
                    case "Darker":
                        mFilterStyle = FilterStyle.DRK;
                        break;

                    case "Brighter":
                        mFilterStyle = FilterStyle.BRT;
                        break;

                    case "Saturate":
                        mFilterStyle = FilterStyle.SAT;
                        break;

                    case "Desaturate":
                        mFilterStyle = FilterStyle.DESAT;
                        break;

                    case "Redscale":
                        mFilterStyle = FilterStyle.RED;
                        break;

                    case "Greenscale":
                        mFilterStyle = FilterStyle.GREEN;
                        break;

                    case "Bluescale":
                        mFilterStyle = FilterStyle.BLUE;
                        break;

                    case "Grayscale":
                        mFilterStyle = FilterStyle.GRAY;
                        break;

                    case "Invert":
                        mFilterStyle = FilterStyle.IVRT;
                        break;

                    default:
                        mFilterStyle = FilterStyle.DRK;
                        break;

                } // end switch
            } //end changed method
        }); //end cbo Filter listener


    }//END INIT




    //Method to draw with the Draw tool
    private void drawMouse() {
        Shape shape = new Circle(xPos, yPos, 0);
        switch (penStyle) {
            case CIR:
                shape = new Circle(xPos, yPos, penSize);
                break;
            case SQR:
                shape = new Rectangle(xPos, yPos, penSize, penSize);
                break;
            default:
                break;

        } //end switch

        //Fill with color
        mColor = cpkColor.getValue();
        shape.setFill(mColor);

        //For display
        ancRoot.getChildren().add(shape);
        drawShapes.add(shape);
    }

    //Buttons are disabled depending on if there are any undos/redos that can be made
    private void disableUnReDoBtns(boolean bUndo, boolean bRedo) {
        btnUndo.setDisable(bUndo);
        btnRdo.setDisable(bRedo);
    }

    //Buttons are dis/enanbled based on which mouse tool is selected
    private void disableDrawSel(boolean bSel, boolean bDraw) {
        //Mouse tool: if DRAW is selected, disable SELECT
        sldSize.setDisable(bSel);
        tgbCircle.setDisable(bSel);
        tgbSquare.setDisable(bSel);

        //Mouse tool: if SELECT is selected, disable DRAW
        cboFilter.setDisable(bDraw);
        btnFilter.setDisable(bDraw);
        btnBucket.setDisable(bDraw);
    }

    //Buttons are disabled until an image is loaded
    private void disableImageBtns(boolean bImageBtns) {
        btnBlur.setDisable(bImageBtns);
        btnShadow.setDisable(bImageBtns);
        btnSepia.setDisable(bImageBtns);
    }

    //Captures the imageview
    private void snapshotParams() {
        snapshotParameters = new SnapshotParameters();
        snapshotParameters.setViewport(new Rectangle2D(0, 0, imgView.getFitWidth(), imgView.getFitHeight()));
        snapshot = ancRoot.snapshot(snapshotParameters, null);
    }

}
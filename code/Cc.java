package code;

import javafx.scene.SnapshotParameters;
import javafx.scene.effect.Effect;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

import java.util.LinkedList;
import java.util.Queue;
import java.util.function.BiFunction;
import java.util.function.UnaryOperator;

public class Cc {
    //Instance member
    private static Cc stateManager;


    private Stage mMainStage, mSaturationStage;
    private ImageView imgView; // Value injected by FXMLLoader

    //History edits
    private static LinkedList<Image> backImages;
    public static final int MAX_UNDOS = 10;
    private Image img, imgUndo, imgRedo;
    private boolean bAtMostRecentImg = true;

    //Private constructor for Singleton
    private Cc(){backImages = new LinkedList<>();}

    //Static method to get the instance
    public static Cc getInstance(){
        if(stateManager == null){
            stateManager = new Cc();
            backImages = new LinkedList<>();
        }
        return stateManager;
    }




    //from Horstmann
    public static Image transform(Image in, UnaryOperator<Color> f) {
        int width = (int) in.getWidth();
        int height = (int) in.getHeight();
        WritableImage out = new WritableImage(
                width, height);
        for (int x = 0; x < width; x++)
            for (int y = 0; y < height; y++)
                out.getPixelWriter().setColor(x, y,
                        f.apply(in.getPixelReader().getColor(x, y)));
        return out;
    }

    public static <T> Image transform(Image in, BiFunction<Color, T, Color> f, T arg) {
        int width = (int) in.getWidth();
        int height = (int) in.getHeight();
        WritableImage out = new WritableImage(
                width, height);
        for (int x = 0; x < width; x++)
            for (int y = 0; y < height; y++)
                out.getPixelWriter().setColor(x, y,
                        f.apply(in.getPixelReader().getColor(x, y), arg));
        return out;
    }

    //For color effects
    public static Image transform(Image in, ColorTransformer f) {
        int width = (int) in.getWidth();
        int height = (int) in.getHeight();
        WritableImage out = new WritableImage(
                width, height);
        for (int x = 0; x < width; x++)
            for (int y = 0; y < height; y++)
                out.getPixelWriter().setColor(x, y,
                        f.apply(x, y, in.getPixelReader().getColor(x, y)));
        return out;
    }

    //For other effects
    //Blur whole image and not the selection area
    public static Image transform(Image in, Effect effect) {
        Rectangle rect = new Rectangle(0,0, (int) in.getWidth(), (int) in.getHeight());
        rect.setFill(new ImagePattern(in));
        rect.setEffect(effect);

        Image out = rect.snapshot(new SnapshotParameters(), null);

        return out;
    }



    public Stage getMainStage() {
        return mMainStage;
    }

    public void setMainStage(Stage mMainStage) {
        this.mMainStage = mMainStage;
    }

    public Stage getSaturationStage() {
        return mSaturationStage;
    }

    public void setSaturationStage(Stage mSaturationStage) {
        this.mSaturationStage = mSaturationStage;
    }

    public ImageView getImgView() {
        return imgView;
    }

    public void setImgView(ImageView imgView) {
        this.imgView = imgView;
    }

    public Image getImg() {
        return img;
    }

    public void addImgToHistory(Image img) { //changes were made to the image space
        System.out.println("adding image to history. Size" + backImages.size());
        if (bAtMostRecentImg) {
            if (backImages.size() < MAX_UNDOS) {
                backImages.add(img);
            } else { //queue is MAX_UNDOS, have to remove the first image that was added
                backImages.removeFirst();
                backImages.add(img);
            } //end size if
        } else { // Edits will be discarded
            int i = backImages.lastIndexOf(this.img);
            if (i > 0) {
                for (int j = backImages.size() -1; j > i; j--) {
                    System.out.println("Removing img at index: " + j);
                    backImages.remove(j);
                }
                backImages.add(img);
            }
            bAtMostRecentImg = true;
        } //end at the most recent img if

    }


    public void undo(){
        bAtMostRecentImg = false;
        if (imgUndo != null){
            //Get and set the last image
            this.img = imgUndo;
            imgView.setImage(img);

            //Reset imgUndo
            int index = backImages.lastIndexOf(imgUndo);

            if (index > 0) {
                imgUndo = backImages.get(index-1);
            } else {
                imgUndo = null;
            }
        }
    }


    public void redo(){
        int index = backImages.lastIndexOf(img);
        if (index != backImages.size() -1 && index >= 0) { //make sure it's not -1 and the img exists in the list

            System.out.println("index of current img:" + backImages.indexOf(img));
            System.out.println("size of queue:" + backImages.size());
            imgUndo = this.img;
            this.img = backImages.get(index+1);
            imgView.setImage(this.img);
        }
        else if (index == backImages.size() -1) {
            System.out.println("At most recent img");
            bAtMostRecentImg = true;
        }

   }



    public void setImageAndRefreshView(Image img){
        //add to backImages queue
        addImgToHistory(img);

        //set the most previous image
        imgUndo = this.img;

        //set the current image state to this image
        this.img = img;
        imgView.setImage(img);
    }


    public void resetHistory() {
        backImages.clear();
    }


    public void close(){
        imgView.setImage(null);
    }
}
